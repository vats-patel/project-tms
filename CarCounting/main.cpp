#include <windows.h>
#include <stdio.h>
#include <string.h>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#define CV_BGR2GRAY cv::COLOR_BGRA2GRAY
#include<iostream>
#include<conio.h>
#include "Blob.h"
#define SHOW_STEPS

const cv::Scalar SCALAR_BLACK = cv::Scalar(0.0, 0.0, 0.0);
const cv::Scalar SCALAR_WHITE = cv::Scalar(255.0, 255.0, 255.0);
const cv::Scalar SCALAR_YELLOW = cv::Scalar(0.0, 255.0, 255.0);
const cv::Scalar SCALAR_GREEN = cv::Scalar(0.0, 200.0, 0.0);
const cv::Scalar SCALAR_RED = cv::Scalar(0.0, 0.0, 255.0);

void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &existingBlobs, std::vector<Blob> &currentFrameBlobs);
void addBlobToExistingBlobs(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs, int &intIndex);
void addNewBlob(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs);
double distanceBetweenPoints(cv::Point point1, cv::Point point2);
void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName);
void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName);
bool checkIfBlobsCrossedTheLine(std::vector<Blob> &blobs, int &intHorizontalLinePosition, int &carCount);
void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy);
void drawCarCountOnImage(int &carCount, cv::Mat &imgFrame2Copy);
void sendingCount(int count, HANDLE& serialHandle) {
	char lpBuffer[2];
	if (count >= 45) {
		lpBuffer[0] = 'A';
	}
	else if (count >= 20) {
		lpBuffer[0] = 'B';
	}
	else if (count == 0) {
		lpBuffer[0] = 'D';
	}
	else {
		lpBuffer[0] = 'C';
	}
	lpBuffer[1] = '\0';
	DWORD  dNoOFBytestoWrite;
	DWORD  dNoOfBytesWritten = 0;
	dNoOFBytestoWrite = sizeof(lpBuffer);
	char data[256];
	BOOL Status = WriteFile(serialHandle,
		lpBuffer,
		dNoOFBytestoWrite,
		&dNoOfBytesWritten,
		NULL);
	if (Status == TRUE)
		printf("\n\n    %s - Written to ", lpBuffer);
	else
		printf("\n\n   Error %d in Writing to Serial Port", GetLastError());
}

class Signal
{
public:
	char signal[70];

}sign[4];

int main(void) {

	HANDLE serialHandle;
	serialHandle = CreateFile("\\\\.\\COM4", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	BOOL Status;
	if (serialHandle == INVALID_HANDLE_VALUE)
		std::cout<<"\n   Error! - Port %s can't be opened";
	else
		std::cout<<"\n   Port RS232 Opened\n ";

	DCB dcbSerialParams = { 0 };
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	Status = GetCommState(serialHandle, &dcbSerialParams);

	if (Status == FALSE)
		printf("\n   Error! in GetCommState()");

	dcbSerialParams.BaudRate = CBR_9600;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = ONESTOPBIT;
	dcbSerialParams.Parity = NOPARITY;
	Status = SetCommState(serialHandle, &dcbSerialParams);  //Configuring the port according to settings in DCB

	if (Status == FALSE)
		printf("\n   Error! in Setting DCB Structure");
	else{
		printf("\n   Setting DCB Structure Successfull\n");
		printf("\n   Baudrate = %d", dcbSerialParams.BaudRate);
		printf("\n   ByteSize = %d", dcbSerialParams.ByteSize);
		printf("\n   StopBits = %d", dcbSerialParams.StopBits);
		printf("\n   Parity   = %d", dcbSerialParams.Parity);
	}
	COMMTIMEOUTS timeouts = { 0 };
	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier = 10;

	if (SetCommTimeouts(serialHandle, &timeouts) == FALSE)
		printf("\n   Error! in Setting Time Outs");
	else
		printf("\n\n   Setting Serial Port Timeouts Successfull");

	strcpy_s(sign[0].signal, "signal01.mp4");
	strcpy_s(sign[1].signal, "signal02.mp4");
	strcpy_s(sign[2].signal, "signal03.mp4");
	strcpy_s(sign[3].signal, "signal04.mp4");
	int cc = 1;
	char chCheckForEscKey = 0;
	while (cc < 2) {
		cv::VideoCapture capVideo, capVideo1, capVideo2, capVideo3;

		cv::Mat imgFrame1;
		cv::Mat imgFrame2;
		cv::Mat imgFrame3;
		cv::Mat imgFrame4;
		cv::Mat imgFrame5;
		cv::Mat imgFrame6;
		cv::Mat imgFrame7;
		cv::Mat imgFrame8;

		std::vector<Blob> blobs, blobs1, blobs2, blobs3;
		cv::Point crossingLine[2], crossingLine1[2], crossingLine2[2], crossingLine3[2];
		int carCount = 0, carCount1 = 0, carCount2 = 0, carCount3 = 0;

		capVideo.open(sign[0].signal);
		capVideo1.open(sign[1].signal);
		capVideo2.open(sign[2].signal);
		capVideo3.open(sign[3].signal);

		if (!capVideo.isOpened() && !capVideo1.isOpened() && !capVideo2.isOpened() && !capVideo3.isOpened()) {                                                 // if unable to open video file
			std::cout << "error reading video file" << std::endl << std::endl;      // show error message
			_getch();                   // it may be necessary to change or remove this line if not using Windows
			return(0);                                                              // and exit program
		}

		if (capVideo.get(cv::CAP_PROP_FRAME_COUNT) < 2 && capVideo1.get(cv::CAP_PROP_FRAME_COUNT) < 2 && capVideo2.get(cv::CAP_PROP_FRAME_COUNT) < 2 && capVideo3.get(cv::CAP_PROP_FRAME_COUNT) < 2) {
			std::cout << "error: video file must have at least two frames";
			_getch();                   // it may be necessary to change or remove this line if not using Windows
			return(0);
		}

		capVideo.read(imgFrame1);
		capVideo.read(imgFrame2);
		capVideo1.read(imgFrame3);
		capVideo1.read(imgFrame4);
		capVideo2.read(imgFrame5);
		capVideo2.read(imgFrame6);
		capVideo3.read(imgFrame7);
		capVideo3.read(imgFrame8);

		int intHorizontalLinePosition = (int)std::round((double)imgFrame1.rows * 0.35);
		int intHorizontalLinePosition1 = (int)std::round((double)imgFrame3.rows * 0.35);
		int intHorizontalLinePosition2 = (int)std::round((double)imgFrame5.rows * 0.35);
		int intHorizontalLinePosition3 = (int)std::round((double)imgFrame7.rows * 0.35);

		crossingLine[0].x = 0;
		crossingLine[0].y = intHorizontalLinePosition;
		crossingLine[1].x = imgFrame1.cols - 1;
		crossingLine[1].y = intHorizontalLinePosition;

		crossingLine1[0].x = 0;
		crossingLine1[0].y = intHorizontalLinePosition1;
		crossingLine1[1].x = imgFrame3.cols - 1;
		crossingLine1[1].y = intHorizontalLinePosition1;

		crossingLine2[0].x = 0;
		crossingLine2[0].y = intHorizontalLinePosition2;
		crossingLine2[1].x = imgFrame5.cols - 1;
		crossingLine2[1].y = intHorizontalLinePosition2;

		crossingLine3[0].x = 0;
		crossingLine3[0].y = intHorizontalLinePosition3;
		crossingLine3[1].x = imgFrame7.cols - 1;
		crossingLine3[1].y = intHorizontalLinePosition3;

		bool blnFirstFrame = true, blnFirstFrame1 = true, blnFirstFrame2 = true, blnFirstFrame3 = true;
		int frameCount = 2, frameCount1 = 2, frameCount2 = 2, frameCount3 = 2;
		while ((capVideo.isOpened() && chCheckForEscKey != 27) || (capVideo1.isOpened() && chCheckForEscKey != 27) || (capVideo2.isOpened() && chCheckForEscKey != 27) || (capVideo3.isOpened() && chCheckForEscKey != 27)) {
			std::vector<Blob> currentFrameBlobs, currentFrameBlobs1, currentFrameBlobs2, currentFrameBlobs3;

			cv::Mat imgFrame1Copy = imgFrame1.clone();
			cv::Mat imgFrame2Copy = imgFrame2.clone();
			cv::Mat imgFrame3Copy = imgFrame3.clone();
			cv::Mat imgFrame4Copy = imgFrame4.clone();
			cv::Mat imgFrame5Copy = imgFrame5.clone();
			cv::Mat imgFrame6Copy = imgFrame6.clone();
			cv::Mat imgFrame7Copy = imgFrame7.clone();
			cv::Mat imgFrame8Copy = imgFrame8.clone();

			cv::Mat imgDifference, imgDifference1, imgDifference2, imgDifference3;
			cv::Mat imgThresh, imgThresh1, imgThresh2, imgThresh3;

			cv::cvtColor(imgFrame1Copy, imgFrame1Copy, CV_BGR2GRAY);
			cv::cvtColor(imgFrame2Copy, imgFrame2Copy, CV_BGR2GRAY);

			cv::cvtColor(imgFrame3Copy, imgFrame3Copy, CV_BGR2GRAY);
			cv::cvtColor(imgFrame4Copy, imgFrame4Copy, CV_BGR2GRAY);

			cv::cvtColor(imgFrame5Copy, imgFrame5Copy, CV_BGR2GRAY);
			cv::cvtColor(imgFrame6Copy, imgFrame6Copy, CV_BGR2GRAY);

			cv::cvtColor(imgFrame7Copy, imgFrame7Copy, CV_BGR2GRAY);
			cv::cvtColor(imgFrame8Copy, imgFrame8Copy, CV_BGR2GRAY);


			cv::GaussianBlur(imgFrame1Copy, imgFrame1Copy, cv::Size(5, 5), 0);
			cv::GaussianBlur(imgFrame2Copy, imgFrame2Copy, cv::Size(5, 5), 0);

			cv::GaussianBlur(imgFrame3Copy, imgFrame3Copy, cv::Size(5, 5), 0);
			cv::GaussianBlur(imgFrame4Copy, imgFrame4Copy, cv::Size(5, 5), 0);

			cv::GaussianBlur(imgFrame5Copy, imgFrame5Copy, cv::Size(5, 5), 0);
			cv::GaussianBlur(imgFrame6Copy, imgFrame6Copy, cv::Size(5, 5), 0);

			cv::GaussianBlur(imgFrame7Copy, imgFrame7Copy, cv::Size(5, 5), 0);
			cv::GaussianBlur(imgFrame8Copy, imgFrame8Copy, cv::Size(5, 5), 0);

			cv::absdiff(imgFrame1Copy, imgFrame2Copy, imgDifference);
			cv::absdiff(imgFrame3Copy, imgFrame4Copy, imgDifference1);
			cv::absdiff(imgFrame5Copy, imgFrame6Copy, imgDifference2);
			cv::absdiff(imgFrame7Copy, imgFrame8Copy, imgDifference3);

			cv::threshold(imgDifference, imgThresh, 30, 255.0, cv::THRESH_BINARY);
			cv::threshold(imgDifference1, imgThresh1, 30, 255.0, cv::THRESH_BINARY);
			cv::threshold(imgDifference2, imgThresh2, 30, 255.0, cv::THRESH_BINARY);
			cv::threshold(imgDifference3, imgThresh3, 30, 255.0, cv::THRESH_BINARY);

			cv::Mat structuringElement3x3 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
			cv::Mat structuringElement5x5 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
			cv::Mat structuringElement7x7 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));
			cv::Mat structuringElement15x15 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));

			cv::Mat structuringElement3x31 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
			cv::Mat structuringElement5x51 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
			cv::Mat structuringElement7x71 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));
			cv::Mat structuringElement15x151 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));

			cv::Mat structuringElement3x32 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
			cv::Mat structuringElement5x52 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
			cv::Mat structuringElement7x72 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));
			cv::Mat structuringElement15x152 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));

			cv::Mat structuringElement3x33 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
			cv::Mat structuringElement5x53 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
			cv::Mat structuringElement7x73 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));
			cv::Mat structuringElement15x153 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));

			for (unsigned int i = 0; i < 2; i++) {
				cv::dilate(imgThresh, imgThresh, structuringElement5x5);
				cv::dilate(imgThresh, imgThresh, structuringElement5x5);
				cv::erode(imgThresh, imgThresh, structuringElement5x5);

				cv::dilate(imgThresh1, imgThresh1, structuringElement5x51);
				cv::dilate(imgThresh1, imgThresh1, structuringElement5x51);
				cv::erode(imgThresh1, imgThresh1, structuringElement5x51);

				cv::dilate(imgThresh2, imgThresh2, structuringElement5x52);
				cv::dilate(imgThresh2, imgThresh2, structuringElement5x52);
				cv::erode(imgThresh2, imgThresh2, structuringElement5x52);

				cv::dilate(imgThresh3, imgThresh3, structuringElement5x53);
				cv::dilate(imgThresh3, imgThresh3, structuringElement5x53);
				cv::erode(imgThresh3, imgThresh3, structuringElement5x53);
			}

			cv::Mat imgThreshCopy = imgThresh.clone();
			cv::Mat imgThreshCopy1 = imgThresh1.clone();
			cv::Mat imgThreshCopy2 = imgThresh2.clone();
			cv::Mat imgThreshCopy3 = imgThresh3.clone();

			std::vector<std::vector<cv::Point> > contours, contours1, contours2, contours3;

			cv::findContours(imgThreshCopy, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
			cv::findContours(imgThreshCopy1, contours1, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
			cv::findContours(imgThreshCopy2, contours2, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
			cv::findContours(imgThreshCopy3, contours3, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

			std::vector<std::vector<cv::Point> > convexHulls(contours.size());
			std::vector<std::vector<cv::Point> > convexHulls1(contours1.size());
			std::vector<std::vector<cv::Point> > convexHulls2(contours2.size());
			std::vector<std::vector<cv::Point> > convexHulls3(contours3.size());

			for (unsigned int i = 0; i < contours.size(); i++) {
				cv::convexHull(contours[i], convexHulls[i]);
			}
			for (unsigned int i = 0; i < contours1.size(); i++) {
				cv::convexHull(contours1[i], convexHulls1[i]);
			}
			for (unsigned int i = 0; i < contours2.size(); i++) {
				cv::convexHull(contours2[i], convexHulls2[i]);
			}
			for (unsigned int i = 0; i < contours3.size(); i++) {
				cv::convexHull(contours3[i], convexHulls3[i]);
			}

			for (auto &convexHull : convexHulls) {
				Blob possibleBlob(convexHull);
				if (possibleBlob.currentBoundingRect.area() > 400 &&
					possibleBlob.dblCurrentAspectRatio > 0.2 &&
					possibleBlob.dblCurrentAspectRatio < 4.0 &&
					possibleBlob.currentBoundingRect.width > 30 &&
					possibleBlob.currentBoundingRect.height > 30 &&
					possibleBlob.dblCurrentDiagonalSize > 60.0 &&
					(cv::contourArea(possibleBlob.currentContour) / (double)possibleBlob.currentBoundingRect.area()) > 0.50) {
					currentFrameBlobs.push_back(possibleBlob);
				}
			}

			for (auto &convexHull1 : convexHulls1) {
				Blob possibleBlob1(convexHull1);
				if (possibleBlob1.currentBoundingRect.area() > 400 &&
					possibleBlob1.dblCurrentAspectRatio > 0.2 &&
					possibleBlob1.dblCurrentAspectRatio < 4.0 &&
					possibleBlob1.currentBoundingRect.width > 30 &&
					possibleBlob1.currentBoundingRect.height > 30 &&
					possibleBlob1.dblCurrentDiagonalSize > 60.0 &&
					(cv::contourArea(possibleBlob1.currentContour) / (double)possibleBlob1.currentBoundingRect.area()) > 0.50) {
					currentFrameBlobs1.push_back(possibleBlob1);
				}
			}

			for (auto &convexHull2 : convexHulls2) {
				Blob possibleBlob2(convexHull2);
				if (possibleBlob2.currentBoundingRect.area() > 400 &&
					possibleBlob2.dblCurrentAspectRatio > 0.2 &&
					possibleBlob2.dblCurrentAspectRatio < 4.0 &&
					possibleBlob2.currentBoundingRect.width > 30 &&
					possibleBlob2.currentBoundingRect.height > 30 &&
					possibleBlob2.dblCurrentDiagonalSize > 60.0 &&
					(cv::contourArea(possibleBlob2.currentContour) / (double)possibleBlob2.currentBoundingRect.area()) > 0.50) {
					currentFrameBlobs2.push_back(possibleBlob2);
				}
			}

			for (auto &convexHull3 : convexHulls3) {
				Blob possibleBlob3(convexHull3);
				if (possibleBlob3.currentBoundingRect.area() > 400 &&
					possibleBlob3.dblCurrentAspectRatio > 0.2 &&
					possibleBlob3.dblCurrentAspectRatio < 4.0 &&
					possibleBlob3.currentBoundingRect.width > 30 &&
					possibleBlob3.currentBoundingRect.height > 30 &&
					possibleBlob3.dblCurrentDiagonalSize > 60.0 &&
					(cv::contourArea(possibleBlob3.currentContour) / (double)possibleBlob3.currentBoundingRect.area()) > 0.50) {
					currentFrameBlobs3.push_back(possibleBlob3);
				}
			}

			if (blnFirstFrame == true) {
				for (auto &currentFrameBlob : currentFrameBlobs) {
					blobs.push_back(currentFrameBlob);
				}
			}
			else {
				matchCurrentFrameBlobsToExistingBlobs(blobs, currentFrameBlobs);
			}

			if (blnFirstFrame1 == true) {
				for (auto &currentFrameBlob1 : currentFrameBlobs1) {
					blobs1.push_back(currentFrameBlob1);
				}
			}
			else {
				matchCurrentFrameBlobsToExistingBlobs(blobs1, currentFrameBlobs1);
			}

			if (blnFirstFrame2 == true) {
				for (auto &currentFrameBlob2 : currentFrameBlobs2) {
					blobs2.push_back(currentFrameBlob2);
				}
			}
			else {
				matchCurrentFrameBlobsToExistingBlobs(blobs2, currentFrameBlobs2);
			}

			if (blnFirstFrame3 == true) {
				for (auto &currentFrameBlob3 : currentFrameBlobs3) {
					blobs3.push_back(currentFrameBlob3);
				}
			}
			else {
				matchCurrentFrameBlobsToExistingBlobs(blobs3, currentFrameBlobs3);
			}

			imgFrame2Copy = imgFrame2.clone();     // get another copy of frame 2 since we changed the previous frame 2 copy in the processing above
			imgFrame4Copy = imgFrame4.clone();     // get another copy of frame 4 since we changed the previous frame 4 copy in the processing above
			imgFrame6Copy = imgFrame6.clone();     // get another copy of frame 6 since we changed the previous frame 6 copy in the processing above
			imgFrame8Copy = imgFrame8.clone();     // get another copy of frame 8 since we changed the previous frame 8 copy in the processing above

			drawBlobInfoOnImage(blobs, imgFrame2Copy);
			
			bool blnAtLeastOneBlobCrossedTheLine = checkIfBlobsCrossedTheLine(blobs, intHorizontalLinePosition, carCount);
			bool blnAtLeastOneBlobCrossedTheLine1 = checkIfBlobsCrossedTheLine(blobs1, intHorizontalLinePosition1, carCount1);
			bool blnAtLeastOneBlobCrossedTheLine2 = checkIfBlobsCrossedTheLine(blobs2, intHorizontalLinePosition2, carCount2);
			bool blnAtLeastOneBlobCrossedTheLine3 = checkIfBlobsCrossedTheLine(blobs3, intHorizontalLinePosition3, carCount3);

			if (blnAtLeastOneBlobCrossedTheLine == true) {
				cv::line(imgFrame2Copy, crossingLine[0], crossingLine[1], SCALAR_GREEN, 2);
			}
			else {
				cv::line(imgFrame2Copy, crossingLine[0], crossingLine[1], SCALAR_RED, 2);
			}

			if (blnAtLeastOneBlobCrossedTheLine1 == true) {
				cv::line(imgFrame4Copy, crossingLine1[0], crossingLine1[1], SCALAR_GREEN, 2);
			}
			else {
				cv::line(imgFrame4Copy, crossingLine1[0], crossingLine1[1], SCALAR_RED, 2);
			}

			if (blnAtLeastOneBlobCrossedTheLine2 == true) {
				cv::line(imgFrame6Copy, crossingLine2[0], crossingLine2[1], SCALAR_GREEN, 2);
			}
			else {
				cv::line(imgFrame6Copy, crossingLine2[0], crossingLine2[1], SCALAR_RED, 2);
			}

			if (blnAtLeastOneBlobCrossedTheLine3 == true) {
				cv::line(imgFrame8Copy, crossingLine3[0], crossingLine3[1], SCALAR_GREEN, 2);
			}
			else {
				cv::line(imgFrame8Copy, crossingLine3[0], crossingLine3[1], SCALAR_RED, 2);
			}

			drawCarCountOnImage(carCount, imgFrame2Copy);
			drawCarCountOnImage(carCount1, imgFrame4Copy);
			drawCarCountOnImage(carCount2, imgFrame6Copy);
			drawCarCountOnImage(carCount3, imgFrame8Copy);

			cv::imshow("Signal 1", imgFrame2Copy);
			cv::imshow("Signal 2", imgFrame4Copy);
			cv::imshow("Signal 3", imgFrame6Copy);
			cv::imshow("Signal 4", imgFrame8Copy);

			currentFrameBlobs.clear();
			currentFrameBlobs1.clear();
			currentFrameBlobs2.clear();
			currentFrameBlobs3.clear();

			imgFrame1 = imgFrame2.clone();           // move frame 1 up to where frame 2 is
			imgFrame3 = imgFrame4.clone();           // move frame 3 up to where frame 4 is
			imgFrame5 = imgFrame6.clone();           // move frame 5 up to where frame 6 is
			imgFrame7 = imgFrame8.clone();           // move frame 7 up to where frame 8 is

			if (((capVideo.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo.get(cv::CAP_PROP_FRAME_COUNT)) || ((capVideo1.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo1.get(cv::CAP_PROP_FRAME_COUNT)) || ((capVideo2.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo2.get(cv::CAP_PROP_FRAME_COUNT)) || ((capVideo3.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo3.get(cv::CAP_PROP_FRAME_COUNT))) {
				if ((capVideo.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo.get(cv::CAP_PROP_FRAME_COUNT)) {
					capVideo.read(imgFrame2);
				}
				if ((capVideo1.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo1.get(cv::CAP_PROP_FRAME_COUNT)) {
					capVideo1.read(imgFrame4);
				}
				if ((capVideo2.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo2.get(cv::CAP_PROP_FRAME_COUNT)) {
					capVideo2.read(imgFrame6);
				}
				if ((capVideo3.get(cv::CAP_PROP_POS_FRAMES) + 1) < capVideo3.get(cv::CAP_PROP_FRAME_COUNT)) {
					capVideo3.read(imgFrame8);
				}
			}
			else {
				std::cout << "end of video\n";
				break;
			}

			blnFirstFrame = false;
			blnFirstFrame1 = false;
			blnFirstFrame2 = false;
			blnFirstFrame3 = false;

			frameCount++;
			frameCount1++;
			frameCount2++;
			frameCount3++;

			chCheckForEscKey = cv::waitKey(1);
		}
		sendingCount(carCount, serialHandle);
		sendingCount(carCount1, serialHandle);
		sendingCount(carCount2, serialHandle);
		sendingCount(carCount3, serialHandle);

		cc++;
	}
	if (chCheckForEscKey != 27) {               // if the user did not press esc (i.e. we reached the end of the video)
		cv::waitKey(0);                        // hold the windows open to allow the "end of video" message to show
	}

	return(0);
}

void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &existingBlobs, std::vector<Blob> &currentFrameBlobs) {
	for (auto &existingBlob : existingBlobs) {
		existingBlob.blnCurrentMatchFoundOrNewBlob = false;
		existingBlob.predictNextPosition();
	}
	for (auto &currentFrameBlob : currentFrameBlobs) {
		int intIndexOfLeastDistance = 0;
		double dblLeastDistance = 100000.0;
		for (unsigned int i = 0; i < existingBlobs.size(); i++) {
			if (existingBlobs[i].blnStillBeingTracked == true) {
				double dblDistance = distanceBetweenPoints(currentFrameBlob.centerPositions.back(), existingBlobs[i].predictedNextPosition);
				if (dblDistance < dblLeastDistance) {
					dblLeastDistance = dblDistance;
					intIndexOfLeastDistance = i;
				}
			}
		}
		if (dblLeastDistance < currentFrameBlob.dblCurrentDiagonalSize * 0.5) {
			addBlobToExistingBlobs(currentFrameBlob, existingBlobs, intIndexOfLeastDistance);
		}
		else {
			addNewBlob(currentFrameBlob, existingBlobs);
		}
	}
	for (auto &existingBlob : existingBlobs) {
		if (existingBlob.blnCurrentMatchFoundOrNewBlob == false) {
			existingBlob.intNumOfConsecutiveFramesWithoutAMatch++;
		}
		if (existingBlob.intNumOfConsecutiveFramesWithoutAMatch >= 5) {
			existingBlob.blnStillBeingTracked = false;
		}
	}
}

void addBlobToExistingBlobs(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs, int &intIndex) {
	existingBlobs[intIndex].currentContour = currentFrameBlob.currentContour;
	existingBlobs[intIndex].currentBoundingRect = currentFrameBlob.currentBoundingRect;
	existingBlobs[intIndex].centerPositions.push_back(currentFrameBlob.centerPositions.back());
	existingBlobs[intIndex].dblCurrentDiagonalSize = currentFrameBlob.dblCurrentDiagonalSize;
	existingBlobs[intIndex].dblCurrentAspectRatio = currentFrameBlob.dblCurrentAspectRatio;
	existingBlobs[intIndex].blnStillBeingTracked = true;
	existingBlobs[intIndex].blnCurrentMatchFoundOrNewBlob = true;
}

void addNewBlob(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs) {
	currentFrameBlob.blnCurrentMatchFoundOrNewBlob = true;
	existingBlobs.push_back(currentFrameBlob);
}

double distanceBetweenPoints(cv::Point point1, cv::Point point2) {
	int intX = abs(point1.x - point2.x);
	int intY = abs(point1.y - point2.y);
	return(sqrt(pow(intX, 2) + pow(intY, 2)));
}

void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName) {
	cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);
	cv::drawContours(image, contours, -1, SCALAR_WHITE, -1);
	cv::imshow(strImageName, image);
}

void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName) {
	cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);
	std::vector<std::vector<cv::Point> > contours;
	for (auto &blob : blobs) {
		if (blob.blnStillBeingTracked == true) {
			contours.push_back(blob.currentContour);
		}
	}
	cv::drawContours(image, contours, -1, SCALAR_WHITE, -1);
	cv::imshow(strImageName, image);
}

bool checkIfBlobsCrossedTheLine(std::vector<Blob> &blobs, int &intHorizontalLinePosition, int &carCount) {
	bool blnAtLeastOneBlobCrossedTheLine = false;
	for (auto blob : blobs) {
		if (blob.blnStillBeingTracked == true && blob.centerPositions.size() >= 2) {
			int prevFrameIndex = (int)blob.centerPositions.size() - 2;
			int currFrameIndex = (int)blob.centerPositions.size() - 1;
			if (blob.centerPositions[prevFrameIndex].y > intHorizontalLinePosition && blob.centerPositions[currFrameIndex].y <= intHorizontalLinePosition) {
				carCount++;
				blnAtLeastOneBlobCrossedTheLine = true;
			}
		}
	}
	return blnAtLeastOneBlobCrossedTheLine;
}

void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy) {
	for (unsigned int i = 0; i < blobs.size(); i++) {
		if (blobs[i].blnStillBeingTracked == true) {
			cv::rectangle(imgFrame2Copy, blobs[i].currentBoundingRect, SCALAR_RED, 2);
			int intFontFace = cv::FONT_HERSHEY_SIMPLEX;
			double dblFontScale = blobs[i].dblCurrentDiagonalSize / 60.0;
			int intFontThickness = (int)std::round(dblFontScale * 1.0);
			cv::putText(imgFrame2Copy, std::to_string(i), blobs[i].centerPositions.back(), intFontFace, dblFontScale, SCALAR_GREEN, intFontThickness);
		}
	}
}

void drawCarCountOnImage(int &carCount, cv::Mat &imgFrame2Copy) {
	int intFontFace = cv::FONT_HERSHEY_SIMPLEX;
	double dblFontScale = (imgFrame2Copy.rows * imgFrame2Copy.cols) / 300000.0;
	int intFontThickness = (int)std::round(dblFontScale * 1.5);
	cv::Size textSize = cv::getTextSize(std::to_string(carCount), intFontFace, dblFontScale, intFontThickness, 0);
	cv::Point ptTextBottomLeftPosition;
	ptTextBottomLeftPosition.x = imgFrame2Copy.cols - 1 - (int)((double)textSize.width * 1.25);
	ptTextBottomLeftPosition.y = (int)((double)textSize.height * 1.25);
	cv::putText(imgFrame2Copy, std::to_string(carCount), ptTextBottomLeftPosition, intFontFace, dblFontScale, SCALAR_GREEN, intFontThickness);
}
