int i,j;
struct signal {
  int red,yellow,green;
}s[4];
int count1,count2,del = 1000;
void setup(){
  Serial.begin(9600);
  for(i=0;i<4;i++){
    s[i].red = 2 + (3*i);
    s[i].green = 4 + (3*i);
  }
  for(i=0;i<4;i++){
    pinMode(s[i].red,OUTPUT);
    pinMode(s[i].green,OUTPUT);
  }
}

void loop(){
  if(Serial.available()){
   for(i=0;i<4;i++){
      for(j=0;j<4;j++){
        if(i==j){
          digitalWrite(s[i].green,HIGH);
          digitalWrite(s[i].red,LOW);
        }
        else{
          digitalWrite(s[j].green,LOW);
          digitalWrite(s[j].red,HIGH);
        }
      }
      char option = Serial.read();
      if(option == 'A'){
        del = 8000;
      }
      else if(option == 'B'){
        del = 4000;
      }
      else if(option == 'C'){
        del = 2000;
      }
      else if(option == 'D'){
        Serial.read();
        continue;
      }
      delay(del);
      Serial.read();
    }
  }
  else{
    for(i=0;i<4;i++){
      for(j=0;j<4;j++){
        if(i==j){
          digitalWrite(s[i].green,HIGH);
          digitalWrite(s[i].red,LOW);
        }
        else{
          digitalWrite(s[j].green,LOW);
          digitalWrite(s[j].red,HIGH);
        }
      }
      delay(1000);
    }
  }
}
